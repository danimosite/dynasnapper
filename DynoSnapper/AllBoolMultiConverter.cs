﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace DynoSnapper
{
    public class AllBoolMultiConverter : IMultiValueConverter
    {
        public static AllBoolMultiConverter Instance = new AllBoolMultiConverter();
        enum Parameters
        {
            Normal,
            Inverted
        }

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var v = values.ToList().All(x => (bool)x);


            var direction = parameter == null ?
                Parameters.Normal :
                (Parameters)Enum.Parse(typeof(Parameters), (string)parameter);

            if (direction == Parameters.Inverted)
                v = !v;

            return v;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
