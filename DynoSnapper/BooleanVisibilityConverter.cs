﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace DynoSnapper
{
    public class BooleanVisibilityConverter : IValueConverter
    {
        public static BooleanVisibilityConverter Instance = new BooleanVisibilityConverter();

        enum Parameters
        {
            Normal,
            Inverted
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var v = (bool)value;

            var direction = parameter == null ?
                Parameters.Normal :
                (Parameters)Enum.Parse(typeof(Parameters), (string)parameter);

            if (direction == Parameters.Inverted)
                v = !v;

            return v ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
