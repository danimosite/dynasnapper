﻿using System;

using Dynamo.Wpf.Extensions;

namespace DynoSnapper
{
    public class EntryPoint : IViewExtension
    {

        #region Properties

        public string UniqueId => Guid.NewGuid().ToString();

        // Edit string when starting a new ViewExtension...
        public string Name => "DynoSnapper";

        private Snapper m_snapper;

        #endregion

        #region Methods

        public void Startup(ViewStartupParams p) { }

        public void Loaded(ViewLoadedParams p)
        {
            m_snapper = new Snapper(p);
        }

        public void Dispose() { }

        public void Shutdown() { }

        #endregion
    }
}
