﻿using System;
using System.IO;

namespace DynoSnapper
{
    public class ImageExportOptions
    {
        public string RootDirectory { get; private set; }
        public string RootSaveDirectory { get; private set; }
        public bool PreserveDirectoryStructure { get; private set; }

        public ImageExportOptions(string rootDirectory, string rootSaveDirectory, bool preserveDirectoryStructure)
        {
            RootDirectory = rootDirectory ?? throw new ArgumentNullException(nameof(rootDirectory));

            RootSaveDirectory = rootSaveDirectory;
            PreserveDirectoryStructure = preserveDirectoryStructure;

            if (string.IsNullOrEmpty(RootSaveDirectory))
                RootSaveDirectory = RootDirectory;
        }

        public string GetImageFilepath(string dynamoFilepath)
        {
            var imageFileName = $"{Path.GetFileNameWithoutExtension(dynamoFilepath)}.png";

            if (PreserveDirectoryStructure)
            {
                if (RootDirectory == RootSaveDirectory)
                {
                    var dir = Path.Combine(new FileInfo(dynamoFilepath).DirectoryName, "Images");

                    if (!Directory.Exists(dir))
                        Directory.CreateDirectory(dir);

                    return Path.Combine(dir, imageFileName);
                }

                var path = RootSaveDirectory;

                var relPath = new FileInfo(dynamoFilepath).DirectoryName.Replace(RootDirectory, "");

                if (!string.IsNullOrEmpty(relPath))
                {
                    path = Path.Combine(path, relPath.TrimStart(new char[1] { Path.DirectorySeparatorChar }));

                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);
                }

                return Path.Combine(path, imageFileName);
            }

            if (RootDirectory == RootSaveDirectory)
            {
                var dir = Path.Combine(RootSaveDirectory, "Images");

                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);

                return Path.Combine(dir, imageFileName);
            }

            return Path.Combine(RootSaveDirectory, imageFileName);
        }

        private static string GetRelativePath(string rootPath, string toPath)
        {
            if (string.IsNullOrEmpty(rootPath)) throw new ArgumentNullException("rootPath");
            if (string.IsNullOrEmpty(toPath)) throw new ArgumentNullException("toPath");

            Uri fromUri = new Uri(rootPath);
            Uri toUri = new Uri(toPath);

            if (fromUri.Scheme != toUri.Scheme) { return toPath; } // path can't be made relative.

            Uri relativeUri = fromUri.MakeRelativeUri(toUri);
            string relativePath = Uri.UnescapeDataString(relativeUri.ToString());

            if (toUri.Scheme.Equals("file", StringComparison.InvariantCultureIgnoreCase))
            {
                relativePath = relativePath.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
            }

            return relativePath;
        }

    }
}
