﻿SETUP
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
If you are using this from a Visual Studio Template, you will need to do the following...
• Right click on References > Manage NuGet Packages and restore all packages.

If you are NOT using this from a Template, be sure to do the following...

• Rename Solution
• Rename Project
• Right click on References > Manage NuGet Packages and reload all packages.

For instructions of how to rename a Visual Studio Solution/Project see this video...
https://youtu.be/Q6R1_MdV6To

In both cases you will need to do the following...
• Rename all Classes, files, folders and Namespaces to suit your Project requirements...
	- Make sure that you update the _ViewExtensionDefinition.xml file accordingly. The name of this file should be the same as whatever you name the
	  UserViewExtension.cs Class suffixed by "_ViewExtensionDefinition.xml".

	  Inside the body of the .xml file you will see the following...

		  <ViewExtensionDefinition>
			<AssemblyPath>..\DynoSnapper.dll</AssemblyPath>
			<TypeName>DynoSnapper.UserViewExtension</TypeName>
		  </ViewExtensionDefinition>

		  This is in the format of...

		  <ViewExtensionDefinition>
			<AssemblyPath>..\ProjectName.dll</AssemblyPath>
			<TypeName>ProjectName.ClassName</TypeName>
		  </ViewExtensionDefinition>

		  Where the ProjectName is the name of your Solution/Project and the ClassName is the class that gets executed (in this case OOTB it will be the UserViewExtension.cs class)

	- Make sure when renaming namespaces, that you rename the folders to be the same and that you also update the XAML namespaces.

• Be sure to check that the correct Dynamo NuGet packages are loaded for your ViewExtension. The ones loaded into this Template are for version 2.0.2, you will need to update this
  if you are building to other versions of Dynamo.

• Ensure all references have 'Copy Local' set to False.



BUILD SETUP AND DEBUGGING
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
COPYING VIEW EXTENSION FILES POST BUILD

If you are building out to Dynamo, you can add the following to the Project Properties > Build Events > Post-Build Event Command Line (be sure to use the correct version of Dynamo)...

	xcopy /Y  "$(TargetDir)*.dll*" "C:\Program Files\Dynamo\Dynamo Core\2"
	xcopy /Y  "$(TargetDir)*.xml*" "C:\Program Files\Dynamo\Dynamo Core\2\viewExtensions"

This will copy the .dll and .xml to the correct locations.

NOTE:
• These are copying to the Dynamo 2.0 version folders, you will need to change the path accordingly for different versions of Dynamo.
• You will either need to be running Visual Studio with Administrator priveleges or remove User Restrictions on the C:\Program Files\Dynamo\Dynamo Core\ directory or the build
  will fail since the directory is restricted to Admin priveleges .

DEBUGGING WITH REVIT/DYNAMO SANDBOX

If you are building out to Dynamo for Revit then you can start Revit by adding the following to Project Properties > Debug > Start External > Program...

	C:\Program Files\Autodesk\Revit 2018\Revit.exe

If you are building out to Dynamo Sandbox, then add this instead...

	C:\Program Files\Dynamo\Dynamo Core\2\DynamoSandbox.exe

Again, make sure that in both cases that you check the path exists and that your're debugging in the correct version of the application.