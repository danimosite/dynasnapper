﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

using Dynamo.Graph.Workspaces;
using Dynamo.ViewModels;
using Dynamo.Wpf.Extensions;

namespace DynoSnapper
{
    public class Snapper
    {
        private ViewLoadedParams _viewLoadedParams;
        public ViewLoadedParams ViewLoadedParams => _viewLoadedParams;

        private DynamoViewModel _dynamoViewModel;
        public DynamoViewModel DynamoViewModel => _dynamoViewModel;

        private SnapperViewModel _snapperViewModel;
        public SnapperViewModel SnapperViewModel => _snapperViewModel;

        private System.Windows.Controls.MenuItem m_exportWizard;


        private bool m_isWorkspaceOpened = false;

        public Snapper(ViewLoadedParams p)
        {
            _viewLoadedParams = p;
            _dynamoViewModel = (DynamoViewModel)p.DynamoWindow.DataContext;

            _snapperViewModel = new SnapperViewModel(this);

            CreateMenus();
        }

        //public async Task OnExportAllInDirectory(bool recursive = false)
        //{
        //    var fbd = new FolderBrowserDialog();

        //    if (fbd.ShowDialog() == DialogResult.OK)
        //        await ExportAllInDirectory(fbd.SelectedPath, recursive);
        //}

        /// <summary>
        /// Opens and exports images of all Dynamo Files in the selected Directory.
        /// </summary>
        ///<remarks>
        /// This method is asynchronous as to avoid blocking UI and stopping the graphical workspace from displaying as we need to export the graphical workspace.
        /// </remarks>
        public async Task ExportAllInDirectory(ImageExportOptions options, bool incSubFolders, string directory = "") //string directoryPath, string savePath = "", bool incSubFolders = false, bool keepDirectoryStructure = false)
        {
            if (string.IsNullOrEmpty(directory))
                directory = options.RootDirectory;

            var dirInfo = new DirectoryInfo(directory);

            var files = new DirectoryInfo(directory).GetFiles("*.dyn").ToList();

            if (files == null || files.Count() == 0)
                return;

            foreach (var f in files)
            {
                // We need to listen for changes to the DynamoViewModel.ShowStartPage property which we can do by Subbing to PropertyChanged event... 
                _dynamoViewModel.PropertyChanged += OnDynamoViewModelPropertyChanged;

                // Open the Dynamo File with Run Mode set to Manual to avoid Scripts running on open...
                _dynamoViewModel.OpenCommand.Execute(new Tuple<string, bool>(f.FullName, true));

                // Here we wait until the Workspace is fully loaded and showing in Dynamo. We do this in an Asynchronous fashion to allow Dynamo UI to load the visual workspace.
                // NOTE: I really don't like this solution, but I can't seem to find a nicer way to be notified when workspace is open and visible.
                // In future versions of Dynamo there is a DynamoModel.WorkspaceOpened event which looks promising.
                while (!m_isWorkspaceOpened)
                    await Task.Delay(5);

                // We can now export the image since the Workspace is Opened and is visible in Dynamo...
                ExportImage(options, f.FullName);
                m_isWorkspaceOpened = false;
            }

            if (incSubFolders)
                foreach (var d in dirInfo.GetDirectories())
                    await ExportAllInDirectory(options, incSubFolders, d.FullName);
        }

        /// <summary>
        /// If the Image can be exported.
        /// </summary>
        public bool CanExportImage() => !_dynamoViewModel.ShowStartPage && !string.IsNullOrEmpty(_dynamoViewModel.CurrentSpace.FileName);

        /// <summary>
        /// Zooms Extents of Workspace and exports an image.
        /// </summary>
        public void ExportImage(ImageExportOptions options, string filepath)
        {
            if (!CanExportImage())
                return;

            _dynamoViewModel.FitViewCommand.Execute(null);
            _dynamoViewModel.SaveImage(options.GetImageFilepath(filepath));
        }

        /// <summary>
        /// Called when the <see cref="Dynamo.ViewModels.DynamoViewModel"/> Property Changed event is fired.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDynamoViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // We want to see when the ShowStartPage property is changed, this gives a rough indicator when a workspace is loaded and a good entry point to avoid interupting the workspace render...
            if (e.PropertyName == nameof(_dynamoViewModel.ShowStartPage) && !_dynamoViewModel.ShowStartPage)
            {
                _dynamoViewModel.PropertyChanged -= OnDynamoViewModelPropertyChanged;

                // Here we will listen for the CurrentOffsetChanged event as this is the last event called in the workspace when loading the workspace.
                // Since we are exporting an image, we need the workspace to actually be rendered in the canvas.
                _dynamoViewModel.CurrentSpace.CurrentOffsetChanged += OnOffsetChanged;
            }
        }

        /// <summary>
        /// When the Workspaces CurrentOffset is changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnOffsetChanged(object sender, EventArgs e)
        {
            _dynamoViewModel.CurrentSpace.CurrentOffsetChanged -= OnOffsetChanged;
            m_isWorkspaceOpened = true;
        }

        /// <summary>
        /// Create Menus in Dynamo Menu.
        /// </summary>
        private void CreateMenus()
        {
            var dynMenu = _viewLoadedParams.dynamoMenu;

            // Create DynoSnapper Menu in Main Menu...
            var snapperMenu = new System.Windows.Controls.MenuItem()
            {
                Header = "DynoSnapper"
            };

            dynMenu.Items.Add(snapperMenu);

            m_exportWizard = new System.Windows.Controls.MenuItem()
            {
                Header = "Image Exporter",
                ToolTip = "Opens the Image Exporter Wizard",
            };
            snapperMenu.Items.Add(m_exportWizard);

            m_exportWizard.Click += (sender, args) => SnapperViewModel.Show();
        }
    }
}
