﻿using System;
using System.Windows;

namespace DynoSnapper
{
    /// <summary>
    /// Interaction logic for SnapperView.xaml
    /// </summary>
    public partial class SnapperView : Window
    {
        private SnapperViewModel _VM;

        public SnapperView(SnapperViewModel snapperViewModel)
        {
            InitializeComponent();

            _VM = snapperViewModel;
            DataContext = _VM;
            Owner = _VM.Snapper.ViewLoadedParams.DynamoWindow;
            WindowStartupLocation = WindowStartupLocation.CenterOwner;

            Closed += OnWindowClosed;
        }

        private void OnWindowClosed(object sender, EventArgs e) => _VM.OnViewClosed();

        private void tbx_RootDir_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e) => _VM?.OnRootDirectoryTextChanged(tbx_RootDir.Text);

        private void tbx_ImgDir_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e) => _VM?.OnImageDirectoryTextChanged(tbx_ImgDir.Text);
    }
}
