﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace DynoSnapper
{
    public class SnapperViewModel : INotifyPropertyChanged
    {

        private Snapper _snapper;
        public Snapper Snapper => _snapper;

        public bool IsViewOpen => View != null;

        private SnapperView _view;
        public SnapperView View => _view;


        private bool _isValidRootDirectory = true;
        public bool IsValidRootDirectory
        {
            get => _isValidRootDirectory;
            private set
            {
                if (value != _isValidRootDirectory)
                {
                    _isValidRootDirectory = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _rootDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        public string RootDirectory
        {
            get => _rootDirectory;
            set
            {
                if (!string.IsNullOrEmpty(value) && value != _rootDirectory && Directory.Exists(value))
                {
                    _rootDirectory = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool _isValidImageDirectory = true;
        public bool IsValidImageDirectory
        {
            get => _isValidImageDirectory;
            private set
            {
                if (value != _isValidImageDirectory)
                {
                    _isValidImageDirectory = value;
                    OnPropertyChanged();
                }
            }
        }

        private string _imageDirectory;
        public string ImageDirectory
        {
            get => _imageDirectory;
            set
            {
                if (value != _imageDirectory)
                {
                    if (!string.IsNullOrEmpty(value) && !Directory.Exists(value))
                        return;

                    _imageDirectory = value;
                    OnPropertyChanged();
                }
            }
        }

        //private bool _createImageFolder = true;
        //public bool CreateImageFolder
        //{
        //    get => _createImageFolder;
        //    set
        //    {
        //        if (value != _createImageFolder)
        //        {
        //            _createImageFolder = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}

        private bool _includeSubfolders;
        public bool IncludeSubfolders
        {
            get => _includeSubfolders;
            set
            {
                if (value != _includeSubfolders)
                {
                    _includeSubfolders = value;

                    if (!_includeSubfolders)
                        PreserveDirectoryStructure = false;

                    OnPropertyChanged();
                }
            }
        }

        private bool _preserveDirectoryStructure;
        public bool PreserveDirectoryStructure
        {
            get => _preserveDirectoryStructure;
            set
            {
                if (value != _preserveDirectoryStructure)
                {
                    _preserveDirectoryStructure = value;
                    OnPropertyChanged();
                }
            }
        }

        public SnapperViewModel(Snapper snapper)
        {
            _snapper = snapper;

            ExportCommand = new RelayCommand(OnExport, CanExport);
            SelectRootFolderCommand = new RelayCommand(OnSelectRootFolder, CanSelectRootFolder);
            SelectImageFolderCommand = new RelayCommand(OnSelectImageFolder, CanSelectImageFolder);
            CloseCommand = new RelayCommand(OnCloseView, CanCloseView);
        }

        private bool CanShow() => !IsViewOpen;

        public void Show()
        {
            if (!CanShow())
                return;

            _view = new SnapperView(this);
            _view.Show();
        }

        private bool CanExport() => true;
        private void OnExport()
        {
            if (!CanExport())
                return;

            var options = new ImageExportOptions(RootDirectory, ImageDirectory, PreserveDirectoryStructure);

            _snapper.ExportAllInDirectory(options, IncludeSubfolders);

            OnCloseView();
        }

        private bool CanSelectRootFolder() => true;
        private void OnSelectRootFolder()
        {
            if (!CanSelectRootFolder())
                return;

            RootDirectory = SelectFolder(RootDirectory);
        }

        private bool CanSelectImageFolder() => true;
        private void OnSelectImageFolder()
        {
            if (!CanSelectImageFolder())
                return;

            var defaultPath = string.IsNullOrEmpty(ImageDirectory) ? RootDirectory : ImageDirectory;
            ImageDirectory = SelectFolder(defaultPath);
        }

        private string SelectFolder(string defaultPath)
        {
            var fbd = new System.Windows.Forms.FolderBrowserDialog();

            if (!string.IsNullOrEmpty(defaultPath))
                fbd.SelectedPath = defaultPath;

            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                return fbd.SelectedPath;

            return defaultPath;
        }

        public void OnRootDirectoryTextChanged(string value)
        {
            IsValidRootDirectory = Directory.Exists(value) ? ValidateRootDirectory(new DirectoryInfo(value)) : false;
        }

        private bool ValidateRootDirectory(DirectoryInfo dirInfo)
        {
            bool isValid = false;

            var files = dirInfo.GetFiles("*.dyn");

            if (files.Count() > 0)
                return true;

            foreach (var d in dirInfo.GetDirectories())
            {
                isValid = ValidateRootDirectory(d);

                if (isValid)
                    return true;
            }

            return isValid;
        }

        public void OnImageDirectoryTextChanged(string value)
        {
            IsValidImageDirectory = string.IsNullOrEmpty(value) || Directory.Exists(value);
        }

        private bool CanCloseView() => true;
        private void OnCloseView()
        {
            if (!CanCloseView() || View == null)
                return;

            View.Close();
        }

        public void OnViewClosed()
        {
            _view = null;
        }

        public RelayCommand ExportCommand { get; private set; }
        public RelayCommand SelectRootFolderCommand { get; private set; }
        public RelayCommand SelectImageFolderCommand { get; private set; }
        public RelayCommand CloseCommand { get; private set; }

        protected void OnPropertyChanged([CallerMemberName] string name = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
